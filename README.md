# backyardWatcher

## Name
The backyard watcher.

## Description
This program allows to point a camera to your backyard (or anywhere else) and save images only when the scene changes.
It should capture wild animals, or anything else moving.

## Installation
This program requires `python3` with the following dependencies:
- `opencv` (`apt-get install python3-opencv`)
- `scikit-image` (`pip3 install scikit-image`)

## Usage
The simplest way to use the program is to launch it through the command line, with a command such as
```bash
./backyardWatcher.py
```

You can adjust the verbose level by adding one or more `-v` to your command. The default level (0) should not print
anything unless errors are present. For example, to get a lot of verbose, for example if you want to choose a meaningful
sensitivity for the snapping trigger, use something like
```
./backyardWatcher.py -vv
```

If you have multiple connected cameras, you can select the one you need by specifying its index when starting the
program. For example, if the capture device is identified by your system as `/dev/video1` (so with the index `1`), then
```
./backyardWatcher.py -c 1
```
will select this device. If you have only one capture device, the default index `0` should work just fine.

The sensitivity of the image change needed to trigger a capture can be adjusted using the `-t` option. This corresponds
to the structural similarity threshold value (in [0, 1]), when comparing two consecutive images, under which a change
will be deemed sufficient to save a snapshot. To get more details regarding the used implementation, refer to the
`scikit-image` [documentation](https://scikit-image.org/docs/stable/api/skimage.metrics.html#skimage.metrics.structural_similarity).

Change the period in between each grabbed images by using the `-p PERIOD` option.

Add a pause after an image was saved to the disk by using the `-b PERIOD` option.

Change the output directory using the `-o DIR` option.

Show each image by using the `-s` option.

To quit the program when the `-s` option was set, hit the `Esc` key. Otherwise use `Ctrl-c`.


## Known issues
The current implementation can be laggy.
