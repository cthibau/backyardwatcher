#!/usr/bin/env python3
import os
import cv2
import time
import argparse
import datetime
from skimage.metrics import structural_similarity

def parseCommandLine():
	'''Parse the command line arguments.'''
	parser = argparse.ArgumentParser('Watch a video input and snap pictures on significant changes.')
	arg = parser.add_argument
	arg('-v', '--verbose', action='count', default=0, help='increase verbose, can be used numerous times')
	arg('-p', '--sampling-period', type=float, metavar='PERIOD', default=1, help='snap a picture to compare at this period (default: %(default)ss)')
	arg('-b', '--break-period', type=float, metavar='PERIOD', default=1, help='wait this period after saving a picture (default: %(default)ss)')
	arg('-c', '--camera-index', type=int, metavar='INDEX', default=0, help='camera index (default: %(default)ss)')
	arg('-t', '--ssim-threshold', type=float, metavar='THRESHOLD', default=0.9, help='SSIM detection threshold (default: %(default)ss)')
	arg('-o', '--output-dir', type=str, metavar='DIR', default='/tmp', help='output report directory (default: %(default)s)')
	arg('-s', '--show-images', action='store_true', help='show each sampled images')
	return parser.parse_args()


def arePicturesDifferrent(p1, p2, threshold, verbose):
	if p1 is None or p2 is None:
		return False
	# convert images to grayscale
	p1Gray = cv2.cvtColor(p1, cv2.COLOR_BGR2GRAY)
	p2Gray = cv2.cvtColor(p2, cv2.COLOR_BGR2GRAY)
	# compute SSIM between two images
	(score, diff) = structural_similarity(p1Gray,p2Gray, full=True)
	if verbose > 1:
		print('structural similarity: {:.3f}'.format(score))
	return score < threshold


def savePicture(picture, outputDir, verbose):
	DIR_DATETIME_FORMAT = '%Y-%m-%d.%Hh%M:%S'
	now = datetime.datetime.now()
	formattedTime = now.strftime(DIR_DATETIME_FORMAT)
	filename = os.path.join(outputDir, formattedTime + '.png')
	if verbose > 0:
		print('saving actual picture to {}'.format(filename))
	cv2.imwrite(filename, picture)


def watchBackyard(camera, samplingPeriod, breakPeriod, threshold, outputDir, showImages, verbose):
	'''Watch a video input and snap pictures on significant changes.
	Args:
		camera (cv2.VideoCapture): Capture device.
		samplingPeriod (float): Period at which images are snapped, in seconds.
		breakPeriod (float): Pause after an image is saved, in seconds.
		threshold (float): Structural similarity threshold, [0, 1].
		outputDir (str): Output directory where the images will be saved.
		verbose (int): Verbose level, the higher the chattier.
	'''
	previousPicture = None
	realBreakPeriod = (breakPeriod - samplingPeriod) if (breakPeriod - samplingPeriod) > 0 else 0
	while True:
		ret, actualPicture = camera.read()
		if ret:
			if showImages:
				cv2.imshow('Backyard watcher', actualPicture)
				if cv2.waitKey(1) == 27:
					break
			if arePicturesDifferrent(actualPicture, previousPicture, threshold, verbose):
				savePicture(actualPicture, outputDir, verbose)
				time.sleep(realBreakPeriod)
		else:
			print('error, could not snap actual picture')
		previousPicture = actualPicture
		time.sleep(samplingPeriod)


if __name__ == '__main__':
	args = parseCommandLine()
	# create the output directory if needed
	if not os.path.isdir(args.output_dir):
		os.makedirs(args.output_dir)
	# instanciate the camera object
	camera = cv2.VideoCapture(args.camera_index)
	# launch the main loop
	try:
		watchBackyard(camera, args.sampling_period, args.break_period, args.ssim_threshold, args.output_dir,
		              args.show_images, args.verbose)
	except KeyboardInterrupt:
		pass
	finally:
		if args.verbose > 0:
			print('\ncleaning stuff, bye!')
		camera.release()
		cv2.destroyAllWindows()


